/**************************************************************************//**
 * @file
 * @brief NThis file contains all the members of the BoardTraversal class
 *****************************************************************************/
#include "boardTraversal.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the default constructor for the BoardTraversal class
 *
 * @param[in] g - The grid containing the graph
 * @param[in] w - the width of the game board
 * @param[in] h - the height of the game board
 *
 *****************************************************************************/
BoardTraversal::BoardTraversal(const int *g, int w, int h) : Graph(g, w, h)
{
   boardHeight = h;
   boardWidth = w;
   for(int i = 0; i < h*w; i++)
   {
      visited[i] = false;
      previous[i] = -1;
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will tell you if you can move up from a specific node
 *
 * @param[in] currNode - The node you are trying to move from
 *
 * @return - true if the move is possible, false if not
 *
 *****************************************************************************/
int BoardTraversal::moveUp(int currNode)
{
   int nextNode = currNode+boardWidth;
   set<int> adjacent = adj(currNode);

   if(adjacent.find(nextNode) == adjacent.end() || visited[nextNode])
   {
      nextNode = -1;
   }

   return nextNode;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will tell you if you can move down from a specific node
 *
 * @param[in] currNode - The node you are trying to move from
 *
 * @return - true if the move is possible, false if not
 *
 *****************************************************************************/
int BoardTraversal::moveDown(int currNode)
{
   int nextNode = currNode-boardWidth;
   set<int> adjacent = adj(currNode);

   if(adjacent.find(nextNode) == adjacent.end() || visited[nextNode])
   {
      nextNode = -1;
   }

   return nextNode;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will tell you if you can move left from a specific node
 *
 * @param[in] currNode - The node you are trying to move from
 *
 * @return - true if the move is possible, false if not
 *
 *****************************************************************************/
int BoardTraversal::moveLeft(int currNode)
{
   int nextNode = currNode-1;
   set<int> adjacent = adj(currNode);

   if(adjacent.find(nextNode) == adjacent.end() || visited[nextNode])
   {
      nextNode = -1;
   }

   return nextNode;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will tell you if you can move right from a specific node
 *
 * @param[in] currNode - The node you are trying to move from
 *
 * @return - true if the move is possible, false if not
 *
 *****************************************************************************/
int BoardTraversal::moveRight(int currNode)
{
   int nextNode = currNode+1;
   set<int> adjacent = adj(currNode);

   if(adjacent.find(nextNode) == adjacent.end() || visited[nextNode])
   {
      nextNode = -1;
   }

   return nextNode;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will retrun the x position of a node on the board. It assumes
 * the upper left hand corner is (0, 0).
 *
 * @param[in] node - The node you are geting the position of
 *
 * @return - the x position of the node
 *
 *****************************************************************************/
int BoardTraversal::returnX(int node)
{
   return (node % boardWidth);
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will retrun the y position of a node on the board. It assumes
 * the upper left hand corner is (0, 0).
 *
 * @param[in] node - The node you are geting the position of
 *
 * @return - the y position of the node
 *
 *****************************************************************************/
int BoardTraversal::returnY(int node)
{
   return (boardHeight - 1) - (node / boardWidth);
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will return all the nodes located diagonally from a node
 *
 * @param[in] node - the node in the center
 *
 * @return - a set of all diagonal nodes
 *
 *****************************************************************************/
set<int> BoardTraversal::returnDiag(int node)
{
   set<int> diagonalNodes;

   //check to make sure each diagonal exists before adding them
   if(returnX(node) != 0 && returnY(node) != 0)
   {
      diagonalNodes.insert(node-1-boardWidth);
   }

   if(returnX(node) != boardWidth-1 && returnY(node) != 0)
   {
      diagonalNodes.insert(node+1-boardWidth);
   }

   if(returnX(node) != 0 && returnY(node) != boardHeight-1)
   {
      diagonalNodes.insert(node-1+boardWidth);
   }

   if(returnX(node) != boardWidth-1 && returnY(node) != boardHeight-1)
   {
      diagonalNodes.insert(node+1+boardWidth);
   }
   return diagonalNodes;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function returns the previously node visited from our node
 *
 * @param[in] node - the node we are looking at
 *
 * @return - the previous node
 *
 *****************************************************************************/
int BoardTraversal::returnPrev(int node)
{
   return previous[node];
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function returns a set of all unvisited nodes.
 *
 * @return - the set of nodes
 *
 *****************************************************************************/
set<int> BoardTraversal::returnUnvisited()
{
   set<int> unvisited;

   for(int v : Vertices())
   {
      if(!visited[v] && !adj(v).empty())
      {
         unvisited.insert(v);
      }
   }

   return unvisited;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function returns a set of all the nodes that are connected to any other
 * nodes in the graph. It's just a set of all nodes, excluding lone nodes.
 *
 * @return - the set of nodes
 *
 *****************************************************************************/
set<int> BoardTraversal::returnAllConnectedNodes()
{
   set<int> connectedVert;
   for(int v : Vertices())
   {
      if(!adj(v).empty())
      {
         connectedVert.insert(v);
      }
   }
   return connectedVert;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function returns the height of the game board
 *
 * @return - the height of the board
 *
 *****************************************************************************/
int BoardTraversal::returnHeight()
{
   return boardHeight;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function returns the width of the game board
 *
 * @return - the width of the board
 *
 *****************************************************************************/
int BoardTraversal::returnWidth()
{
   return boardWidth;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function allows you to make a node as being visited and set its previous
 * node. Mostly used for path finding algorithms.
 *
 * @param[in] lastNode - The previous node
 * @param[in] nextNode - the node we are moving to.
 *
 *****************************************************************************/
void BoardTraversal::traverseToNode(int lastNode, int nextNode)
{
   previous[nextNode] = lastNode;
   visited[nextNode] = true;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function allows you to mark a visited node as being unvisited and clear
 * it's previous node. Mostly used for path finding algorithms.
 *
 * @param[in] lastNode - The node we are marking as being unvisited.
 *
 *****************************************************************************/
void BoardTraversal::reverseFromNode(int lastNode)
{
   previous[lastNode] = -1;
   visited[lastNode] = false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will check to see if a node is not able to traverse to another node.
 *
 * @param[in] node - the node we are looking at
 *
 * @return - true if the node is isolated, false if it's not
 *
 *****************************************************************************/
bool BoardTraversal::isDeadEnd(int node)
{
   if(moveUp(node) == -1)
   {
      if(moveDown(node) == -1)
      {
         if(moveRight(node) == -1)
         {
            if(moveLeft(node) == -1)
            {
               return true;
            }
         }
      }
   }

   return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return whether a node is visited or not.
 *
 * @param[in] node - the node we are checking
 *
 * @return - true if it's visited, false if not
 *
 *****************************************************************************/
bool BoardTraversal::isVisited(int node)
{
   return visited[node];
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will tell you if 2 nodes are next to eachother.
 *
 * @param[in] node1 - one of the nodes
 * @param[in] node2 - the other node
 *
 * @return - true if they are adjacent, false if not
 *
 *****************************************************************************/
bool BoardTraversal::isAdjacent(int node1, int node2)
{
   //if node 2 is in node 1's adjacency list, they are adjacent.
   for(int i : adj(node1))
   {
      if(i == node2)
      {
         return true;
      }
   }

   //return false if nothing was found
   return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will convert 2 node values into a valid move. You pass your
 * current node in and the next node you want to go to.
 *
 * @param[in] currNode - the current node
 * @param[in] nextNode - the next node to travel to
 *
 * @return - the move that should be performed
 *
 *****************************************************************************/
ValidMove BoardTraversal::translateNodeToMove(int currNode, int nextNode)
{
   ValidMove move = NONE;
   nextNode = currNode - nextNode;

   if(nextNode == -1)
   {
      move = RIGHT;
   }
   else if(nextNode == 1)
   {
      move = LEFT;
   }
   else if(nextNode == boardWidth)
   {
      move = DOWN;
   }
   else if(nextNode == (-1 * boardWidth))
   {
      move = UP;
   }

   return move;
}
