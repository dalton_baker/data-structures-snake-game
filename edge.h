/**************************************************************************//**
 * @file
 * @brief This is the header file for the Edge class
 *****************************************************************************/
#ifndef	__EDGE__H__
#define __EDGE__H__

#include <iostream>

/*!
* @brief This is the edge class, it is used for the weighted edge graph
*/
class Edge
{
protected:
   /*! This is one of the verticies */
   int vert1;
   /*! This is the other vertex */
   int vert2;
   /*! This is the weight of the edge */
   double weight;
   /*! This is the weight of the edge */
   bool marked;
public:
   Edge();
   Edge(int v1, int v2, double w);
   int either();
   int other(int vert);
   double getWeight();
   void setWeight(double newWeight);
   void changeMark();
   bool isMarked();

   bool operator>(const Edge &e);
   bool operator<(const Edge &e);
   friend std::ostream &operator<<(std::ostream &out, Edge *e);
};


#endif
