/**************************************************************************//**
 * @file
 * @brief This contains all the members for the Edge class
 *****************************************************************************/
#include "edge.h"

/**************************************************************************//**
 * @author Dalton Bakerint
 *
 * @par Description:
 * This is the default edge constructor
 *
 *****************************************************************************/
Edge::Edge()
{
   vert1 = -1;
   vert2 = -1;
   weight = -1;
   marked = false;
}

/**************************************************************************//**
 * @author Dalton Bakerint
 *
 * @par Description:
 * This is the edge constructor with input
 *
 * @param[in] v1 - one of the verticies
 *
 * @param[in] v2 - the other vertex
 *
 * @param[in] w - the weight of the edge
 *
 *****************************************************************************/
Edge::Edge(int v1, int v2, double w)
{
   vert1 = v1;
   vert2 = v2;
   weight = w;
   marked = false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will just return the first edge passed into the function
 *
 * @return - vert1, just one of the verticies
 *
 *****************************************************************************/
int Edge::either()
{
   return vert1;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This returns whichever edge is notr passed into the function.
 *
 * @param[in] vert - whichever vertex you already have the info of
 *
 * @return - the vertex you don't have yet
 *
 *****************************************************************************/
int Edge::other(int vert)
{
   if(vert == vert2)
   {
      return vert1;
   }

   return vert2;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This just gives you the weight of an edge
 *
 * @return - the weight of the edge
 *
 *****************************************************************************/
double Edge::getWeight()
{
   return weight;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This allows you to change the weight of an edge.
 *
 * @param[in] newWeight - The edge on the rhs
 *
 *****************************************************************************/
void Edge::setWeight(double newWeight)
{
   weight = newWeight;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will change the mark of an edge to the negation of itself.
 *
 *****************************************************************************/
void Edge::changeMark()
{
   marked = !marked;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will tell you if an edge is marked.
 *
 * @return - true if the edge is marked, false if not
 *
 *****************************************************************************/
bool Edge::isMarked()
{
   return marked;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is an > overloader for the Edge class
 *
 * @param[in] e - The edge on the rhs
 *
 * @return - true if lhs is greater than than the rhs, false if not
 *
 *****************************************************************************/
bool Edge::operator>(const Edge &e)
{
   //return the result of this comparison
   return weight > e.weight;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is an < overloader for the Edge class
 *
 * @param[in] e - The edge on the rhs
 *
 * @return - true if lhs is less than the rhs, false if not
 *
 *****************************************************************************/
bool Edge::operator<(const Edge &e)
{
   //return what the result of this comparison is
   return weight < e.weight;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is an << overloader for the Edge class, it lets me print it out
 *
 * @param[in] out - The out stream we are printing to
 *
 * @param[in] e - The edge on the rhs
 *
 * @return - The outstream we fred into
 *
 *****************************************************************************/
std::ostream &operator<<(std::ostream &out, Edge *e)
{
   out << e->vert1 << " " << e->vert2 << " " << e->weight;
   return out;
}
