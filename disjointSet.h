/********************************************************************//**
 * @file
 ***********************************************************************/
#ifndef __DISJOINTSET__H__
#define __DISJOINTSET__H__

#include <string.h>   // for memcpy

/*!
* @brief This is the DisjointSet class, it grows as needed
*/
class DisjointSet
{
protected:
   /*! This holds the disjoint set */
   long int *nodes;
   /*! This is the size-1 of the disjoint set */
   long int size;
public:
   DisjointSet();
   ~DisjointSet();
   void Union(long int loc1, long int loc2);
   long int findSet(long int loc) const;
private:
   void growSet(long int newSize);
};
#endif
