#ifndef __GRAPH__H__
#define __GRAPH__H__

#include "edge.h"
#include "disjointSet.h"
#include <iostream>
#include <map>
#include <set>

using namespace std;

/*!
* @brief This is the Graph base class, it is unweighted and undirrected
*/
class Graph
{
protected:
   /*! This holds all of the graph data */
   map<int, set<Edge*>> vertices;
   /*! This holds the number of vertices */
   int numVertices;
   /*! This holds the number of edges */
   int numEdges;
   /*! This holds the disjoint set */
   DisjointSet conectedComp;
public:
   Graph();
   Graph(istream &fin);
   Graph(const int *grid, int width, int height);
   virtual ~Graph();

   set<int> Vertices() const;
   set<int> adj(int v) const;
   set<Edge*> incEdges(int v) const;
   virtual void addEdge(int v1, int v2, double w = 0.0);
   int V() const;
   int E() const;
   bool connected(int v1, int v2) const;
};

#endif
