/**************************************************************************//**
 * @file
 * @brief This file contains the player class declarations
 *****************************************************************************/
#ifndef __PLAYER_H
#define __PLAYER_H
#include "playfield.h"
#include "hamiltonianish.h"

using std::cout;
using std::endl;

/*!
* @brief This is the Hamiltonianish class, it will try to find a hamiltonian
* path through a game board
*/
class Player
{
private:
   /*! This is the hamiltonian path that we will be traversing */
   Hamiltonianish *hamiltonianPath;
   /*! This is the number of moves that has taken place */
   int moveCount;
   /*! The last score of the game board */
   int lastScore;
   /*! This is where the head is on the board */
   int headLocation;
public:
   Player ();
   ValidMove makeMove(Playfield *);
   bool checkValidMove(ValidMove move, const int *grid);
};

#endif
