#include "graph.h"

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * This is the default constructor for a graph
 *
 *****************************************************************************/
Graph::Graph()
{
   numVertices = 0;
   numEdges = 0;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * This is a constructor for a graph using an input stream
 *
 * @param[in] fin - The input stream
 *
 *****************************************************************************/
Graph::Graph(istream &fin)
{
   int v, w;

   while (!fin.eof())
   {
      fin >> v >> w;
      addEdge(v, w);
   }
   //recalculate the number of vertices
   numVertices = vertices.size();
   numEdges = 0;

   //recalculate the number of edges
   for (int u : Vertices())
   {
      numEdges += adj(u).size();
   }

   //device the number of edges by 2
   numEdges /= 2;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is a constructor for a graph using a gameboard grid
 *
 * @param[in] grid - the grid that we are turning into a graph
 *
 * @param[in] width - the width of the grid
 *
 * @param[in] height - the height of the grid
 *
 *****************************************************************************/
Graph::Graph(const int *grid, int width, int height)
{
   //create all the nodes
   for(int i = 0; i < width*height; i++)
   {
      //passing in the same number twice will create a node with no edges
      addEdge(i, i);
   }

   //add all edges that should exist
   for(int i = 0; i < width*height; i++)
   {
      //check the node above the node we are looking at
      if((grid[i+width]%2 == 0) && (grid[i]%2 == 0) && ((i/width) < height-1))
      {
         addEdge(i, i+width);
      }

      //check the node to the left of the node we are looking at
      if((grid[i-1]%2 == 0) && (grid[i]%2 == 0) && ((i%width) != 0))
      {
         addEdge(i, i-1);
      }

      //check the node bellow the node we are looking at
      if((grid[i-width]%2 == 0) && (grid[i]%2 == 0) && ((i/width) != 0))
      {
         addEdge(i, i-width);
      }

      //check the node to the right of the node we are looking at
      if((grid[i+1]%2 == 0) && (grid[i]%2 == 0) && ((i%width) != (width-1)))
      {
         addEdge(i, i+1);
      }
   }

   //recalculate the number of vertices
   numVertices = vertices.size();
   numEdges = 0;

   //recalculate the number of edges
   for (int u : Vertices())
   {
      numEdges += adj(u).size();
   }

   //device the number of edges by 2
   numEdges /= 2;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * frees up all the memory allocate by the graph class
 *
 *****************************************************************************/
Graph::~Graph()
{
   for (int v : Vertices())
   {
      for (Edge* e : vertices[v])
      {
         //remove the edge from the list of edges for each node
         vertices[e->other(v)].erase(e);
         vertices[v].erase(e);

         //delete the edge
         delete e;
      }
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * Returns a set of all vertices in a graph
 *
 * @return - set of all vertices in the graph
 *
 *****************************************************************************/
set<int> Graph::Vertices() const
{
   //create a set to hold all the vertices
   set<int> returnVertices = {};

   //use a range based for loop to grab all of the vertices
   for(auto v : vertices)
   {
      //insert the node into the the set
      //vert.first accesses the node index!!
      returnVertices.insert(v.first);
   }

   //return the set
   return returnVertices;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * Returns a set of all adjacent vertices of a vetex
 *
 * @param[in] v - The vertex we are looking at
 *
 * @return - set of all adjacent vertices
 *
 *****************************************************************************/
set<int> Graph::adj(int v) const
{
   set<int> returnAdj;

   if(vertices.find(v) != vertices.end())
   {
      for( Edge* e : vertices.at(v))
      {
         returnAdj.insert(e->other(v));
      }
   }

   return returnAdj;
}


/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * Returns a set of all edges incident to the provided vertex.
 *
 * @param[in] v - The vertex we are looking at
 *
 * @return - set of all incident edges
 *
 *****************************************************************************/
set<Edge*> Graph::incEdges(int v) const
{
   return vertices.at(v);
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * Adds an edge connecting 2 vertices of a graph. If a vertex doesn't exist
 * it will be created in the process. If you pass in 2 of the same number it
 * it will simply create a single node with no edges.
 *
 * @param[in] v1 - The first vertex for the edge
 *
 * @param[in] v2 - The second vertex for the edge
 *
 * @param[in] w - The weight of the edge. It is always 0.0 for a base class
 *
 *****************************************************************************/
void Graph::addEdge(int v1, int v2, double w)
{
   set<int> addEdgeAdj = adj(v1);

   //if 2 of the same number is passed in, create a single node
   if(v1 == v2)
   {
      vertices[v1];
   }
   //create 2 nodes with an edge between them in every other case
   else if(addEdgeAdj.find(v2) ==  addEdgeAdj.end())
   {
      Edge *newEdge = new Edge(v1, v2, w);
      //Add each node to the other nodes list of connected nodes
      vertices[v1].insert(newEdge);
      vertices[v2].insert(newEdge);
      conectedComp.Union(v1, v2);
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * Returns the total number of vertices in the graph
 *
 * @return - the number of vertices
 *
 *****************************************************************************/
int Graph::V() const
{
   return numVertices;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * Returns the total number of edges in the graph
 *
 * @return - the number of edges
 *
 *****************************************************************************/
int Graph::E() const
{
   return numEdges;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * Given 2 nodes, this will tell you if there is a path between them.
 *
 * @param[in] v1 - one of the vertices
 *
 * @param[in] v2 - the other vertex
 *
 * @return - true if there is a path between the 2 nodes
 *
 *****************************************************************************/
bool Graph::connected(int v1, int v2) const
{
   return conectedComp.findSet(v1) == conectedComp.findSet(v2);
}
