/**************************************************************************//**
 * @file
 * @brief This file contains all the members of the player class
 *****************************************************************************/
#include "player.h"
#include <iostream>

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the default player constructor
 *
 *****************************************************************************/
Player::Player()
{
   hamiltonianPath = nullptr;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the player move function. It will decide the best move to make
 *
 * @param[in] pf - The playfield currently being used
 *
 * @return - the next move to make
 *
 *****************************************************************************/
ValidMove Player::makeMove(Playfield *pf)
{
   ValidMove move = NONE;

   //search the array for the location of the head value
   for(int i = 0; i < PLAYFIELD_HEIGHT*PLAYFIELD_WIDTH; i++)
   {
      if(pf->getGrid()[i] == HEAD_VALUE)
      {
         headLocation = i;
      }
   }

   //create the hamiltonian-ish path object
   if(hamiltonianPath == nullptr)
   {
      hamiltonianPath = new Hamiltonianish(pf->getGrid(),
                                    PLAYFIELD_WIDTH, PLAYFIELD_HEIGHT);
   }

   //if the head is not in the path, give up
   if(!hamiltonianPath->shiftToNode(headLocation))
   {
      if(checkValidMove(RIGHT, pf->getGrid()))
      {
         return RIGHT;
      }
      else if(checkValidMove(LEFT, pf->getGrid()))
      {
         return LEFT;
      }
      else if(checkValidMove(UP, pf->getGrid()))
      {
         return UP;
      }
      else if(checkValidMove(DOWN, pf->getGrid()))
      {
         return DOWN;
      }
   }

   //if we have traversed the board twice without getting food, end the game
   if((lastScore == pf->getScore()) &&
            moveCount > (PLAYFIELD_HEIGHT*PLAYFIELD_WIDTH*2))
   {
      return NONE;
   }

   //if the score updates, reset the count
   if(lastScore != pf->getScore())
   {
      lastScore = pf->getScore();
      moveCount = 0;
   }

   //get a move from the object
   move = hamiltonianPath->nextMove();
   moveCount++;

   return move;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will check if a move will end the game
 *
 * @param[in] move - The move we are checking
 * @param[in] grid - The playfield currently being used
 *
 * @return - true if the move is ok, false if it will end the game
 *
 *****************************************************************************/
bool Player::checkValidMove(ValidMove move, const int *grid)
{
   //if the right move isn't valid retrun false
   if((move == RIGHT) && ((grid[headLocation+1]%4 != 0)
         || (headLocation+1)%PLAYFIELD_WIDTH == 0))
   {
      return false;
   }

   //if the down move isn't valid retrun false
   else if((move == DOWN) && ((grid[headLocation-PLAYFIELD_WIDTH]%4 != 0)
         || (headLocation/PLAYFIELD_WIDTH) == 0))
   {
      return false;
   }

   //if the left move isn't valid retrun false
   else if((move == LEFT) && ((grid[headLocation-1]%4 != 0)
         || (headLocation)%PLAYFIELD_WIDTH == 0))
   {
      return false;
   }

   //if the up move isn't valid retrun false
   else if((move == UP) && ((grid[headLocation+PLAYFIELD_WIDTH]%4 != 0)
         || (headLocation/PLAYFIELD_WIDTH) == (PLAYFIELD_HEIGHT-1)))
   {
      return false;
   }

   return true;
}
