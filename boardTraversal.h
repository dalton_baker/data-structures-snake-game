/**************************************************************************//**
 * @file
 * @brief This is the header file for the BoardTraversal class
 *****************************************************************************/
#ifndef __BOARDTRAVERSAL__H__
#define __BOARDTRAVERSAL__H__
#include "graph.h"
#include "playfield.h"

/*!
* @brief This is the board traversal class, it returns and keeps track of info
*/
class BoardTraversal : public Graph
{
protected:
   /*! This is the height of the board */
   int boardHeight;
   /*! This is the width of the board */
   int boardWidth;
   /*! This keeps track of which nodes are visited */
   map<int, bool> visited;
   /*! This keeps track of the node visited previously to each node */
   map<int, int> previous;

public:
   BoardTraversal(const int *g, int w, int h);

   int moveUp(int currNode);
   int moveDown(int currNode);
   int moveLeft(int currNode);
   int moveRight(int currNode);

   int returnX(int node);
   int returnY(int node);
   set<int> returnDiag(int node);
   int returnPrev(int node);
   set<int> returnUnvisited();
   set<int> returnAllConnectedNodes();
   int returnHeight();
   int returnWidth();

   void traverseToNode(int lastNode, int nextNode);
   void reverseFromNode(int lastNode);

   bool isDeadEnd(int node);
   bool isVisited(int node);
   bool isAdjacent(int node1, int node2);

   ValidMove translateNodeToMove(int currNode, int nextNode);

};

#endif
