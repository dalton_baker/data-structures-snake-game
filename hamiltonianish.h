/**************************************************************************//**
 * @file
 * @brief This is the header file for
 *****************************************************************************/
#ifndef __HAMILTONIANISH__H__
#define __HAMILTONIANISH__H__
#include <deque>
#include "boardTraversal.h"

using std::deque;
using std::cout;
using std::endl;

/*!
* @brief This is the Hamiltonianish class, it will try to find a hamiltonian
* path through a game board
*/
class Hamiltonianish
{
protected:
   /*! This is the base graph this will return info about the game board */
   BoardTraversal *baseGraph;
   /*! This the where the path will start */
   int startNode;
   /*! This is where the path will end */
   int endNode;
   /*! This is the path that will be built */
   deque<int> path;

public:
   Hamiltonianish(const int *g, int w, int h);
   ~Hamiltonianish();
   ValidMove nextMove();
   bool shiftToNode(int node);

private:
   void buildPaths();
   void findStartEndNode();
   void findTopPath();
   void findWindingPath(bool goingDown = true, bool lastMoveLeft = false);
   void backUpPath();

   bool checkFlippyCorner(int &node);
   void fillInDoubUnv();
   void createFlippyPath();

   void insertDouble(int start, int doub1, int doub2, int end);
   set<int> getUnvisitedNodes();

};

#endif
