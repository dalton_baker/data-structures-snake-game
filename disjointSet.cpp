#include "disjointSet.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the default cunstructor. It sets size to 0 and the array to nullptr.
 *
 *****************************************************************************/
DisjointSet::DisjointSet()
{
   //set size to 10
   size = 10;

   //Make a new array
   nodes = new long int[size];

   //fill the array with -1
   for(int i = 0; i < size; i++)
   {
      nodes[i] = -1;
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function de-allocates the array to free up memory.
 *
 *****************************************************************************/
DisjointSet::~DisjointSet()
{
   delete [] nodes;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * Performs the union operation. This will attempt to create a tree that is
 * log(n) deep at the maximum.
 *
 * @param[in] loc1 - The location of the first item we are unioning
 *
 * @param[in] loc2 - The location of the second item we are unioning
 *
 *****************************************************************************/
void DisjointSet::Union(long int loc1, long int loc2)
{
   //check that the locations are in our bounds
   if(loc1 >= size || loc2 >= size)
   {
      growSet(loc1 > loc2 ? loc1+1 : loc2+1);
   }

   //find the root of each set
   loc2 = findSet(loc2);
   loc1 = findSet(loc1);

   //if the roots are the same do nothing
   if(loc1 == loc2)
   {
      return;
   }
   //if loc1's tree is deeper, set loacation 2 equal to it.
   else if(loc1 < loc2)
   {
      nodes[loc2] = loc1;
      nodes[loc1]--;
   }
   //if loc2's tree is deeper, set loacation 1 equal to it.
   else
   {
      nodes[loc1] = loc2;
      nodes[loc2]--;
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * Returns the set that represents a particular item. Returns -1 if the
 * location passed in was invalid.
 *
 * @param[in] loc - The location of the item we are finding the root of
 *
 * @return - The locatation of the root the set of the item passed in
 *
 *****************************************************************************/
long int DisjointSet::findSet(long int loc) const
{
   //check that the locations are in our bounds
   if(loc >= size || loc < 0)
   {
      return loc;
   }

   //follow the location to the root
   while(nodes[loc] >= 0)
   {
      loc = nodes[loc];
   }

   //return the roots location
   return loc;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function grows the disjoint set if it is not big enough
 *
 * @param[in] newSize - The size of the set
 *
 *****************************************************************************/
void DisjointSet::growSet(long int newSize)
{
   //create new array
   long int *newNodes = new long int[newSize];

   //set all new nodes to -1
   for(int i = size-1; i < newSize; i++)
   {
      newNodes[i] = -1;
   }

   //copy old array into the new array
   memcpy(newNodes, nodes, size * sizeof(long int));

   //set new size, delete nodes and set it to the new array
   size = newSize;
   delete []nodes;
   nodes = newNodes;
}
