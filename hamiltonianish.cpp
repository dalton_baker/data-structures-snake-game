/**************************************************************************//**
 * @file
 * @brief This file hold all the members of the hamiltonianish class
 *****************************************************************************/
#include "hamiltonianish.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the default constructor for the hamiltonianish class
 *
 * @param[in] g - The grid we are building a path for
 * @param[in] w - The width of the grid
 * @param[in] h - The height of the grid
 *
 *****************************************************************************/
Hamiltonianish::Hamiltonianish(const int *g, int w, int h)
{
   //create our graph traversal object
   baseGraph = new BoardTraversal(g, w, h);
   //find the start and end nodes
   findStartEndNode();
   //mark the start node as being traveled to from the end node
   baseGraph->traverseToNode(endNode, startNode);
   //put the start node in the path
   path.push_back(startNode);
   //build the paths
   buildPaths();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the default destructor for the hamiltonianish class
 *
 *****************************************************************************/
Hamiltonianish::~Hamiltonianish()
{
   delete baseGraph;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will return the next move that should be done
 *
 * @return - the next move
 *
 *****************************************************************************/
ValidMove Hamiltonianish::nextMove()
{
   //get the node we are currently at
   int current = path.front();

   //add it to the end of the list and remove it from the front
   path.push_back(current);
   path.pop_front();

   //turn the 2 nodes into a Valid move and return it
   return baseGraph->translateNodeToMove(current, path.front());
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will shift the path to the position of the node you pass to it.
 * It will return false if the node you pass in does not exist in the path.
 *
 * @param[in] node - The node you want to shift to
 *
 * @return - true if it found the node in the path, false if it didn't
 *
 *****************************************************************************/
bool Hamiltonianish::shiftToNode(int node)
{
   //if we are already at the node, just return
   if(node == path.front())
   {
      return true;
   }

   //shift the first node in order to make the while loop work
   path.push_back(path.front());
   path.pop_front();

   //shift until you find the node, or until you return to the start
   for(unsigned int i = 0; i < path.size(); i++)
   {
      path.push_back(path.front());
      path.pop_front();
      if(path.front() == node)
      {
         return true;
      }
   }

   return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the function that builds the actual path. First it creates the
 * path that traverses the top of the board. Then it creates the path that
 * fills in the middle of the board. Finally it fills in all double unvisited
 * nodes and flippy corners.
 *
 *****************************************************************************/
void Hamiltonianish::buildPaths()
{
   //build path traversing the top border, then add it to the master path
   findTopPath();

   //Build the path that fills in the middle top of the board
   findWindingPath();

   //fill in all double unvisited nodes possible
   fillInDoubUnv();

   //create the extended path including flippy corners
   createFlippyPath();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will find and set the start and end nodes of the path. The
 * start node will be the fist node available from the top of the board on the
 * left most column. The end node will be the node dirrectly below it.
 *
 *****************************************************************************/
void Hamiltonianish::findStartEndNode()
{
   //assume upper left hand corner is open
   int firstNode = baseGraph->returnWidth() * (baseGraph->returnHeight() - 1);

   //if it's not check each node below it until you find an open node
   while(baseGraph->adj(firstNode).size() == 0)
   {
      firstNode -= baseGraph->returnWidth();
   }

   //retrun the open node
   startNode = firstNode;
   endNode = baseGraph->moveDown(firstNode);
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will find the path that traverses the top most path of the
 * board. It will stop when it reached the right most column of the board.
 *
 *****************************************************************************/
void Hamiltonianish::findTopPath()
{
   //keep moving until we make it to the right edge
   while(baseGraph->returnX(path.back()) != baseGraph->returnWidth()-1)
   {
      int up = baseGraph->moveUp(path.back());
      int down = baseGraph->moveDown(path.back());
      int left = baseGraph->moveLeft(path.back());
      int right = baseGraph->moveRight(path.back());

      //if we can move up, move up
      if(up >= 0)
      {
         baseGraph->traverseToNode(path.back(), up);
         path.push_back(up);
      }
      //if we can't move up, move right
      else if(right >= 0)
      {
         baseGraph->traverseToNode(path.back(), right);
         path.push_back(right);
      }
      //if we can't move up or right, try down
      else if(down >= 0)
      {
         baseGraph->traverseToNode(path.back(), down);
         path.push_back(down);
      }
      //there is nearly no chance of this happening
      else if(left >= 0)
      {
         baseGraph->traverseToNode(path.back(), left);
         path.push_back(left);
      }
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the most important function of all. This function will start where
 * the findTopPath function left off and attempt to fill in the rest of the
 * board with a path. It will fisrt traverse downward until it can't any longer.
 * If it attempts to make 2 left moves in a row, then it will switch dirrection
 * and start going up instead. Each time a move is added to the path or the
 * direction is changed, a recursive call is made.

 * If it runs into a dead end, it will call the backUpPath function to back out
 * and see what can be done. The function is done when it reaches the endNode.
 *
 * @param[in] goingDown - a bool indicating what direction to go
 * @param[in] lastMoveLeft - this tells us if the last move done was left
 *
 *****************************************************************************/
void Hamiltonianish::findWindingPath(bool goingDown, bool lastMoveLeft)
{
   //if you reach the last node we need in the path, stop finding paths
   if(path.back() == endNode)
   {
      return;
   }

   //if you find a dead end, back up and try to find a different path
   if(baseGraph->isDeadEnd(path.back()))
   {
      backUpPath();
   }

   int up = baseGraph->moveUp(path.back());
   int down = baseGraph->moveDown(path.back());
   int left = baseGraph->moveLeft(path.back());
   int right = baseGraph->moveRight(path.back());

   //always go right if it's possible
   if( (right >= 0) )
   {
      baseGraph->traverseToNode(path.back(), right);
      path.push_back(right);
      findWindingPath(goingDown, false);
   }
   //if we are currently traversing upward, try to go up
   else if( (up >= 0) && (!goingDown) )
   {
      baseGraph->traverseToNode(path.back(), up);
      path.push_back(up);
      findWindingPath(goingDown, false);
   }
   //if we are traversing down, try to go down
   else if( (down >= 0) && goingDown )
   {
      baseGraph->traverseToNode(path.back(), down);
      path.push_back(down);
      findWindingPath(goingDown, false);
   }
   //go left if you have to
   else if( (left >= 0) && (!lastMoveLeft) )
   {

      baseGraph->traverseToNode(path.back(), left);
      path.push_back(left);
      findWindingPath(goingDown, true);
   }
   //if it attempts to go left twice in a row, switch dirrection of traversal
   else
   {
      findWindingPath(!goingDown, false);
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will back the path up in the event that it ends up at a dead
 * end.
 *
 *****************************************************************************/
void Hamiltonianish::backUpPath()
{
   //get the top node and pop it off
   int node = path.back();
   path.pop_back();

   //pop nodes off until you find one you can traverse from
   while(baseGraph->isDeadEnd(path.back()))
   {
      baseGraph->reverseFromNode(node);
      node = path.back();
      path.pop_back();
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will check if a node has a flippy corner. If it does it will
 * change the variable passed in to the flippy corner node.
 *
 * @param[in] node - the node we are checking
 *
 * @return - true if the node is a flippy node, false if not
 *
 *****************************************************************************/
bool Hamiltonianish::checkFlippyCorner(int &node)
{
   //get a set of all adjacent nodes to our node
   set<int> adjToNode = baseGraph->adj(node);

   //check each adjacent node for a flippy edge
   for( int i : adjToNode )
   {
      if(adjToNode.find(baseGraph->returnPrev(baseGraph->returnPrev(i)))
                  != adjToNode.end() && baseGraph->returnPrev(i) != -1)
      {
         node = baseGraph->returnPrev(i);
         return true;
      }
   }
   return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will add all the double unvisited node pairs to the path
 * possible.
 *
 *****************************************************************************/
void Hamiltonianish::fillInDoubUnv()
{
   //keep track of if a double was added
   bool addedDoub = true;

   //if there was one added in the last loop try again
   while(addedDoub)
   {
      deque<int> doubNodes;
      set <int> unvisitedNodes = getUnvisitedNodes();
      addedDoub = false;

      //try every possible combination in the unvisited node set
      for(int i : unvisitedNodes)
      {
         for(int j : unvisitedNodes)
         {
            //if 2 nodes are adjacent, add them to the set of pairs
            if(baseGraph->isAdjacent(i, j) && i != j)
            {
               doubNodes.push_back(i);
               doubNodes.push_back(j);
            }
         }
      }

      //search the adjacent nodes of each node
      //we are searching for a situation where the adjacent node's
      //previsous node is an adjacent node to the other node
      while(!doubNodes.empty())
      {
         //get the first pair
         int i = doubNodes.front();
         doubNodes.pop_front();
         int j = doubNodes.front();
         doubNodes.pop_front();

         //check if each pair is able to be added to the path.
         for(int k : baseGraph->adj(i))
         {
            set<int> adjToJ = baseGraph->adj(j);
            if(adjToJ.find(baseGraph->returnPrev(k)) != adjToJ.end()
            && !baseGraph->isVisited(i) && !baseGraph->isVisited(j))
            {
               addedDoub = true;
               insertDouble(baseGraph->returnPrev(k), j, i, k);
            }
         }
      }
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will double the length of the path and replace all flippy
 * corner nodes with their counterparts in the second traversal.
 *
 *****************************************************************************/
void Hamiltonianish::createFlippyPath()
{
   //preserve the current state of the graph
   deque<int> flippyCornerPath = path;
   int newStart = startNode;

   //cycle through every unvisited node, looking for the flippy corners
   for(int i : getUnvisitedNodes())
   {
      int flippyCorner = i;
      //if you find a flippy corner, add it to the path
      if(checkFlippyCorner(flippyCorner))
      {
         //shift to the old node
         shiftToNode(flippyCorner);
         //get rid of the old node
         path.pop_front();
         //replace it with the flippy counterpart
         path.push_back(i);
         //mark the new traversal
         baseGraph->traverseToNode(path.front(), i);
         //mark the new start node if the original start node was replaced
         if(flippyCorner == startNode)
         {
            newStart = i;
         }
      }
   }
   shiftToNode(newStart);

   //combine the 2 paths into one path
   while(!flippyCornerPath.empty())
   {
      path.push_back(flippyCornerPath.front());
      flippyCornerPath.pop_front();
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This function will insert a unvisited node pair into the path.
 *
 * @param[in] start - the insertion point of the pair
 * @param[in] doub1 - the first of the pair to travel to
 * @param[in] doub2 - the second of the pair to travel to
 * @param[in] end - the exit point from the double nodes
 *
 *****************************************************************************/
void Hamiltonianish::insertDouble(int start, int doub1, int doub2, int end)
{
   //get to the location in the path
   shiftToNode(end);

   //push the 2 nodes into the path
   path.push_back(doub1);
   path.push_back(doub2);

   //mark the new path in the board traversal
   baseGraph->traverseToNode(start, doub1);
   baseGraph->traverseToNode(doub1, doub2);
   baseGraph->traverseToNode(doub2, end);

   //return back to the start of the path
   shiftToNode(startNode);
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will get a list of all unvisited nodes. It will also clear all unvisited
 * nodes of any info they contain.
 *
 * @return - the list of unvisited nodes.
 *
 *****************************************************************************/
set<int> Hamiltonianish::getUnvisitedNodes()
{
   set<int> unvisitedNodes = baseGraph->returnAllConnectedNodes();

   for(int i : path)
   {
      unvisitedNodes.erase(i);
   }

   for(int i : unvisitedNodes)
   {
      baseGraph->reverseFromNode(i);
   }

   return unvisitedNodes;
}
